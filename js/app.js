$(function(){

    //////////-----/////////////Cliente///////////-----///////////

  
    $('#form-clientes').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editaCliente.php';
            urlRedir = url_site+'moradores';
        } else{
            url = url_site+'api/cadastraCliente.php';
            urlRedir = url_site+'cadastroMoradores';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', urlRedir);
                } else {
                    myAlert(data.status,data.msg,'main', urlRedir);
                }
            }
        });

        return false;
    });

    //deleta cliente
    $('#listaClientes').on('click','.removerCliente' ,function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaCliente.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    myAlert(data.status,data.msg,'main', url_site+'moradores');
                } else {
                    myAlert(data.status,data.msg,'main');
                }
            }
        })
        return false;
    })

    //////////-----/////////////Alert///////////-----///////////


    //////////-----/////////////Condominio///////////-----///////////

    $('#form-condo').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editaCondo.php';
            urlRedir = url_site+'condominios';
        } else{
            url = url_site+'api/cadastraCondo.php';
            urlRedir = url_site+'cadastroCondominio';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', urlRedir);
                } else {
                    myAlert(data.status,data.msg,'main', urlRedir);
                }
            }
        });

        return false;
    });

    $('#listaCondo').on('click','.removerCondo' ,function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaCondo.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', url_site+'condominios');
                } else {
                    myAlert(data.status,data.msg,'main');
                }
            }
        })
        return false;
    });

    //////////-----/////////////Unidade///////////-----///////////

    $('#form-unidade').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editaUnd.php';
            urlRedir = url_site+'unidade';
        } else{
            url = url_site+'api/cadastraUnd.php';
            urlRedir = url_site+'cadastroUnidade';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', urlRedir);
                } else {
                    myAlert(data.status,data.msg,'main', urlRedir);
                }
            }
        });

        return false;
    });

    $('#listaUnd').on('click','.removerUnd' ,function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaUnd.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', url_site+'unidade');
                } else {
                    myAlert(data.status,data.msg,'main');
                }
            }
        })
        return false;
    });

    //////////-----/////////////Bloco///////////-----///////////

    $('#form-bloco').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editaBloco.php';
            urlRedir = url_site+'bloco';
        } else{
            url = url_site+'api/cadastraBloco.php';
            urlRedir = url_site+'cadastroBloco';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', urlRedir);
                } else {
                    myAlert(data.status,data.msg,'main', urlRedir);
                }
            }
        });

        return false;
    });

    $('#listaBloco').on('click','.removerBloco' ,function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaBloco.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', url_site+'bloco');
                } else {
                    myAlert(data.status,data.msg,'main');
                }
            }
        })
        return false;
    });
 
    //////////-----/////////////Conselho///////////-----///////////

    $('#form-conselho').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editaConselho.php';
            urlRedir = url_site+'conselho';
        } else{
            url = url_site+'api/cadastraConselho.php';
            urlRedir = url_site+'cadastroConselho';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', urlRedir);
                } else {
                    myAlert(data.status,data.msg,'main', urlRedir);
                }
            }
        });

        return false;
    });

    $('#listaConselho').on('click','.removerConselho' ,function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaConselho.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', url_site+'conselho');
                } else {
                    myAlert(data.status,data.msg,'main');
                }
            }
        })
        return false;
    });

    //////////-----/////////////Administradora///////////-----///////////

    $('#form-adm').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editaAdm.php';
            urlRedir = url_site+'administradora';
        } else{
            url = url_site+'api/cadastraAdm.php';
            urlRedir = url_site+'cadastroAdm';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', urlRedir);
                } else {
                    myAlert(data.status,data.msg,'main', urlRedir);
                }
            }
        });

        return false;
    });

    $('#listaAdm').on('click','.removerAdm' ,function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaAdm.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', url_site+'administradora');
                } else {
                    myAlert(data.status,data.msg,'main');
                }
            }
        })
        return false;
    });

    $('.fromCondominio').change(function(){
        selecionado = $(this).val();
        
        $.ajax({
            url: url_site+'api/listBlocos.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado},
            success : function(data){
                selectPopulation('.fromBloco',data.resultSet, 'nomeBloco');
            }
        })

    })

    //chamar unidades

    $('.fromBloco').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listUnidades.php',
            dataType: 'json',
            type: 'POST',
            data: {id: selecionado},
            success: function(data){
                selectPopulation('.fromUnidade', data.resultSet, 'numUnd')
            }
        })
    })

    function selectPopulation(seletor, dados, field){
        
        estrutura = '<option value="">Selecione...</option>';

        for (let i = 0; i < dados.length; i++) {
        

            estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>';
            
        }
        $(seletor).html(estrutura)

    }

    $("#cadastroUsuario").submit(function(){
        var senha = $(this).find('.senha').val();
        var confirmaSenha = $(this).find('.confirmaSenha').val();
        var editar = $(this).find('input[name="u[editar]"]').val();
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editaUsuario.php';
            urlRedir = url_site+'usuarios';
        } else{
            url = url_site+'api/cadastraUsuario.php';
            urlRedir = url_site+'cadastroUsuarios';
        }

        if(senha === confirmaSenha){
            $.ajax({
                url: url, //cuidado
                dataType: 'json',
                type: 'POST',
                data: $(this).serialize(),
                success : function(data){
                    // console.log(data);
                    if(data.status == 'success'){
                        //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                        myAlert(data.status,data.msg,'main', urlRedir);
                    } else {
                        myAlert(data.status,data.msg,'main', urlRedir);
                    }
                }
            });
        }else{
            myAlert('danger','A senhas não conhecidem', 'main');
        }

        return false;
    })

    $('#listaUnd').on('click','.removerUser' ,function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaUsuario.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                // console.log(data);
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status,data.msg,'main', url_site+'usuarios');
                } else {
                    myAlert(data.status,data.msg,'main');
                }
            }
        })
        return false;
    });

    //controla filtro
    $('#filtro').submit(function(){
        var pagina = $('input[name="page"]').val();
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();

        termo1 = (termo1) ? termo1+'/' : '';
        termo2 = (termo2) ? termo2+'/' : ''

        window.location.href = url_site+pagina+'/busca/'+termo1+termo2

        return false;
    })

    $('.termo1, .termo2').on('keyup focusout change',function(){
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();
        if(termo1 || termo2){
            $('button[type="submit"]').prop('disabled', false);
        }else{
            $('button[type="submit"]').prop('disabled', true);
        }
    })

    $('input[name=CPF]').mask('000.000.000-00', {reverse: true});
    $('input[name=cep]').mask('00000-000');
    $('input[name=telefone]').mask('(00) 0000-0000');

});

function myAlert(tipo, mensagem, pai, url){

    url = (url == undefined) ? url == '' : url = url;
    componente = '<div class="alert alert-'+tipo+'" role="alert">'+mensagem+'</div>';

    $(pai).prepend(componente);

    setTimeout(function() {
        $(pai).find('div.alert').remove();
        //vai redir?
        if(tipo == 'success' && url) {
            setTimeout(function(){
                window.location.href = url;
            }, 80);
        }

    }, 2100);

}