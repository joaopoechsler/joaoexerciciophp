<?
session_start();
error_reporting(0);

$localDir = 'joaoexerciciophp/';
$base_url = "http://".$_SERVER['HTTP_HOST'].'/';

$url_site = $base_url.$localDir;

$fullPath = $_SERVER['DOCUMENT_ROOT'].'/';
$fullPath .= $localDir;

$includes = $fullPath.'includes/';
$models = $fullPath.'models/';
$controllers = $fullPath.'controllers/';
$views = $fullPath.'views/';

// echo $url_site;

require $includes."vars.php";
require $models."dao.Class.php";
require $models."administradora.Class.php";
require $models."condominio.Class.php";
require $models."conselho.Class.php";
require $models."bloco.Class.php";
require $models."unidade.Class.php";
require $models."cadastro.Class.php";
require $models."restrito.Class.php";
require $models."usuario.Class.php";
require $models."connectDB.Class.php";

$nav = array(
    'inicio' => 'Dashboard',
);

// 'cadastro' => 'Cadastro',
// 'clientes' => 'Listar Clientes'
// 'cadastroCondominio' => 'Cad. Condominio',
// 'condominios' => 'Condominios'

function dateFormat($d, $tipo = true){ //2022-03-23 16:24:37
    if(!$d){
        return 'Sem data';
    }
    if($tipo){
        $hora = explode(' ',$d);
        $data = explode('-',$hora[0]);
        return $data[2].'/'.$data[1].'/'.$data[0].' '.$hora[1];
    }else{
        $hora = explode(' ',$d);
        $data = explode('/',$hora[0]);
        return $data[2].'-'.$data[1].'-'.$data[0].' '.$hora[1];
    }

}

function legivel($var,$width = '250',$height = '400') {
    echo '<pre style="color: white;">';
    if(is_array($var)) {
        print_r($var);
    } else {
        print($var);
    }
    echo '</pre>';
}

function trataUrl($params = array()){
    $url = (isset($_GET['b'])) ? 'busca/' : '';
    foreach($params as $value){
        $url .= $value.'/';
    }
    return $url;
}

?>