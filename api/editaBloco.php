<?
require "../uteis.php";

$bloco = new bloco();
if ($bloco->editBloco($_POST)) {

    $result = array(
        "status" => "success",
        "msg" => "Seu Registro foi Editado",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "Ocorreu um erro ao editar o Registro",
    );

    echo json_encode($result);
}

?>