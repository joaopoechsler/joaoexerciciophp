<?
require "../uteis.php";

$adm = new administradora();
if ($adm->setAdministradora($_POST)) {
    
    $result = array(
        "status" => "success",
        "msg" => "Seu Registro foi Cadastrado",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "Ocorreu um erro ao cadastrar o Registro",
    );

    echo json_encode($result);
}

?>