<?
require "../uteis.php";

$adm = new administradora();
if ($adm->deletaAdministradora($_POST['id'])) {

    $totalRegistros = $adm->getAdministradora()['totalResults'];

    $result = array(
        "status" => "success",
        "totalRegistros" => ($totalRegistros < 10 ? '0'. $totalRegistros : $totalRegistros),
        "msg" => "Seu Registro foi deletado",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "Ocorreu um erro ao excluir o Registro",
    );

    echo json_encode($result);
}

?>