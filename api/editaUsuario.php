<?
require "../uteis.php";

$user = new usuarios();
$dadosUser = array ();

foreach ($_POST['u'] as $ch=>$value) {
    $dadosUser[$ch] = $value;
}

if ($user->editUser($dadosUser)) {

    $result = array(
        "status" => "success",
        "msg" => "Seu Registro foi Editado",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "Ocorreu um erro ao editar o Registro",
    );

    echo json_encode($result);
}

?>