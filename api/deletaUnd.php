<?
require "../uteis.php";

$unidade = new unidade();
if ($unidade->deletaUnidade($_POST['id'])) {

    $totalRegistros = $unidade->getUnidade()['totalResults'];

    $result = array(
        "status" => "success",
        "totalRegistros" => ($totalRegistros < 10 ? '0'. $totalRegistros : $totalRegistros),
        "msg" => "Seu Registro foi deletado",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "Ocorreu um erro ao excluir o Registro",
    );

    echo json_encode($result);
}

?>