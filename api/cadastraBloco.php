<?
require "../uteis.php";

$bloco = new bloco();
if ($bloco->setBloco($_POST)) {
    
    $result = array(
        "status" => "success",
        "msg" => "Seu Registro foi Cadastrado",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "Ocorreu um erro ao cadastrar o Registro",
    );

    echo json_encode($result);
}

?>