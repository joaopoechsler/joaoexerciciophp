<?
require "../uteis.php";
$usuario = new usuarios();

$isExists = $usuario->userExistis($_POST['u']['usuario']);

if($isExists['resultSet']['usuario']){
    $result = array(
        "status" => "warning",
        "msg" => "Este usuário Já existe"
    );
    echo json_encode($result);
    exit;
}

if($_POST['u']['senha'] == $_POST['cSenha']){

    $dados = array();

    foreach($_POST['u'] as $field=>$value){
        $dados[$field] = ($field == 'senha') ? md5($value) : $value;
    }

    if($usuario->setUser($dados)){
        $result = array(
            "status" => 'success',
            "msg" => "Registro inserido com sucesso."
        );
    
    } else{
        $result = array(
            "status" => 'danger',
            "msg" => "O cadastro não pode ser inserido"
        );
    
    }
    
} else{
    $result = array(
        "status" => "danger",
        "msg" => "As senhas digitadas na confirmação não conferem!"
    );
}
echo json_encode($result);    
?>