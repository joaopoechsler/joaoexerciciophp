<?
require "../uteis.php";

$cliente = new Cadastro();
if ($cliente->setMorador($_POST)) {
    
    $result = array(
        "status" => "success",
        "msg" => "Seu Registro foi Cadastrado",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "Ocorreu um erro ao cadastrar o Registro",
    );

    echo json_encode($result);
}

?>