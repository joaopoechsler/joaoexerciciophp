<?

class Dashboard extends Dao
{

    function __construct()
    {

    }

    function listaMoradoresCondo()
    {
        $qry = 'SELECT 
        cond.nome,
        COUNT(mor.id) as totalMoradores 
        FROM jp_morador AS mor
        LEFT JOIN jp_condominio AS cond 
        ON cond.id = mor.from_condominio
        GROUP BY from_condominio'; 

        return $this->listarData($qry);
    }

    function ultimas5AdmsCadastradas()
    {
        $qry = 'SELECT jp_administradora.nomeAdm
        FROM jp_administradora 
        ORDER BY jp_administradora.dataCadastro DESC LIMIT 5';

        return $this->listarData($qry);

    }

    function totalCadastros()
    {
       $qry = 'SELECT 
       COUNT(jp_administradora.id) AS totalAdm,
       (SELECT COUNT(id) FROM jp_condominio) AS totalCond,
       (SELECT COUNT(id) FROM jp_bloco) AS totalBloco,
       (SELECT COUNT(id) FROM jp_unidade) AS totalUnidades,
       (SELECT COUNT(id) FROM jp_morador) AS totalMorador
       FROM jp_administradora';

       return $this->listarData($qry);
    }
}
