<?
Class bloco extends condominio{
    // public $nome;
    // public $cpf;
    // public $email;
    // public $telefone;
    protected $dadosBloco = array();
    protected $id;

    function __construct(){
    
    }

    function getBloco($id = null){
        $qry = 'SELECT
        condo.id as idCondo,
        condo.nome, bloco.id, 
        bloco.nomeBloco, 
        bloco.Andares,
        bloco.qtUnidadesAndar,
        bloco.dataCadastro
        FROM jp_bloco bloco 
        INNER JOIN jp_condominio condo ON condo.id = bloco.from_condominio';
        
        if($id){
            $qry .= ' WHERE bloco.id='.$id;
            $unique = true;
        }
        
        return $this->listarData($qry, $unique);
    }

    function setBloco($dadosBloco) {
        $values = '';
        $sql = 'INSERT INTO jp_bloco (';
        foreach($dadosBloco as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';

        return $this->insertData($sql);
    
    }

    function getBlocoFromCond($cond){
        $qry = 'SELECT id, nomeBloco FROM jp_bloco bloco WHERE bloco.from_condominio = '.$cond;
        return $this->listarData($qry);
    }

    function editBloco($dadosBloco){
        $sql = 'UPDATE jp_bloco SET';

        foreach($dadosBloco as $ch=>$value){
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .=' WHERE id='.$dadosBloco['editar'];

        return $this->updateData($sql);
    }

    function deletaBloco($id) {
        $sql = 'DELETE FROM jp_bloco WHERE id ='.$id;
        return $this->deletar($sql);
    }
}
?>