<?
Class condominio extends Dao{
    // public $nome;
    // public $cpf;
    // public $email;
    // public $telefone;
    protected $dadosConselho = array();
    protected $id;

    function __construct(){

    }

    function getCondo($id = null){
        $qry = 'SELECT * FROM jp_condominio';

        if ($id) {
            $qry .= ' WHERE id = '.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique, 5);
    }

    function setCondominio($dadosCondo) {
        $values = '';
        $sql = 'INSERT INTO jp_condominio (';
        foreach($dadosCondo as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';

        return $this->insertData($sql);
    }

    function editCondominio($dadosCondo){
        $sql = 'UPDATE jp_condominio SET';

        foreach($dadosCondo as $ch=>$value){
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .=' WHERE id='.$dadosCondo['editar'];

        return $this->updateData($sql);
    }

    function deletaCondominio($id) {
        return $this->deletar("DELETE FROM jp_condominio WHERE id = ".$id);
    }

}
?>