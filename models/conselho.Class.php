<?
Class conselho extends Dao{
    // public $nome;
    // public $cpf;
    // public $email;
    // public $telefone;
    protected $dadosConselho = array();
    protected $id;

    function __construct(){

    }

    function getConselho($id = null){
        $qry = 'SELECT 
        conselho.id,
        condo.nome,
        conselho.NomeFunc,
        conselho.funcao
        FROM jp_conselho conselho
        INNER JOIN jp_condominio condo ON condo.id = conselho.from_condominio';

        if ($id) {
            $qry .= ' WHERE conselho.id = '.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function listarFuncao($id = null) {
        $qry = 'SELECT DISTINCT funcao 
        FROM jp_conselho 
        WHERE funcao';


        $unique = false;
        return $this->listarData($qry, $unique);
    }

    function setConselho($dadosConselho) {
        $values = '';
        $sql = 'INSERT INTO jp_conselho (';
        foreach($dadosConselho as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';

        return $this->insertData($sql);
    }

    function editConselho($dadosConselho){
        $sql = 'UPDATE jp_conselho SET';

        foreach($dadosConselho as $ch=>$value){
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .=' WHERE id='.$dadosConselho['editar'];

        return $this->updateData($sql);
    }

    function deletaConselho($id) {
        return $this->deletar("DELETE FROM jp_conselho WHERE id = ".$id);
    }

}