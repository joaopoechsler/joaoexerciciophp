<?
class Cadastro extends unidade
{
    // public $nome;
    // public $cpf;
    // public $email;
    // public $telefone;
    protected $dados = array();
    protected $id;

    function __construct()
    {
    }

    function getMorador($id = null)
    {
        $qry = 'SELECT morador.id,  
        bloco.nomeBloco, 
        condo.nome,
        und.numUnd,
        morador.from_condominio,
        morador.from_bloco,
        morador.from_unidade,
        morador.nomeMorador,
        morador.dataCadastro,
        morador.cpf,
        morador.email,
        morador.telefone
        FROM jp_morador morador 
        INNER JOIN jp_bloco bloco ON bloco.id = morador.from_bloco 
        INNER JOIN jp_condominio condo ON condo.id = morador.from_condominio
        INNER JOIN jp_unidade und ON und.id = morador.from_unidade';

        $contaTermos = count($this->busca);

        $isNull = false;

        if($contaTermos > 0){
            
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }

            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $qry .= ' WHERE morador.id ='.$id;
            $unique = true;
        }
            return $this->listarData($qry,$unique);
        }

    function setMorador($dados)
    {

        $values = '';
        $sql = 'INSERT INTO jp_morador (';
        foreach ($dados as $ch => $value) {
            $sql .= '`' . $ch . '`, ';
            $values .= "'" . $value . "', ";
        }

        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES (' . rtrim($values, ', ') . ')';

        return $this->insertData($sql);
    }

    function editMorador($dados)
    {

        $sql = 'UPDATE jp_morador SET';

        foreach ($dados as $ch => $value) {
            if ($ch != 'editar') {
                $sql .= "`" . $ch . "` = '" . $value . "', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id=' . $dados['editar'];

        return $this->updateData($sql);
    }

    function deletaMorador($id)
    {

        return $this->deletar("DELETE FROM jp_morador WHERE id = " . $id);
    }
}
