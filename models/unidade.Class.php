<?
Class unidade extends bloco{
    // public $nome;
    // public $cpf;
    // public $email;
    // public $telefone;
    protected $dadosUnd = array();
    protected $id;

    function __construct(){

    }

    function getUnidade($id = null){
        $qry = 'SELECT unidade.id, 
        unidade.numUnd, 
        bloco.nomeBloco, 
        condo.nome, 
        unidade.from_condominio, 
        unidade.from_bloco, 
        unidade.metragem, 
        unidade.qtVagas,
        unidade.dataCadastro
        FROM jp_unidade unidade 
        INNER JOIN jp_bloco bloco ON bloco.id = unidade.from_bloco 
        INNER JOIN jp_condominio condo ON condo.id = unidade.from_condominio';
        if ($id) {
            $qry .= ' WHERE unidade.id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function setUnidade($dadosUnd) {
        $values = '';
        $sql = 'INSERT INTO jp_unidade (';
        foreach($dadosUnd as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';

        return $this->insertData($sql);
    }

    function editUnidade($dadosUnd){

        $sql = 'UPDATE jp_unidade SET';

        foreach($dadosUnd as $ch=>$value){
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .=' WHERE id='.$dadosUnd['editar'];

        return $this->updateData($sql);

    }

    function getUnidadesFromBloco($und){
        $qry = 'SELECT id, numUnd FROM jp_unidade unidade WHERE unidade.from_bloco = '.$und;
        return $this->listarData($qry);
    }

    function deletaUnidade($id) {
        return $this->deletar("DELETE FROM jp_unidade WHERE id = ".$id);
    }
}
?>