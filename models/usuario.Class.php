<?
Class usuarios extends Dao{
    // public $nome;
    // public $cpf;
    // public $email;
    // public $telefone;
    protected $dadosUser = array();
    protected $id;

    function __construct(){

    }

    function getUser($id = null){
        $qry = 'SELECT usr.id,
        usr.nome,
        usr.usuario,
        usr.dataCadastro
        FROM jp_user usr';

        if ($id) {
            $qry .= ' WHERE usr.id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function userExistis($user){
        $qry = "SELECT usuario FROM jp_user WHERE usuario = '".$user."'";
        return $this->listarData($qry,true);
    }

    function setUser($dadosUser) {
        $values = '';
        $sql = 'INSERT INTO jp_user (';
        foreach($dadosUser as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';

        return $this->insertData($sql);
    }

    function editUser($dadosUser){

        $sql = 'UPDATE jp_user SET';

        foreach($dadosUser as $ch=>$value){
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .=' WHERE id='.$dadosUser['editar'];

        return $this->updateData($sql);

    }

    function deletaUser($id) {
        return $this->deletar("DELETE FROM jp_user WHERE id = ".$id);
    }
}
?>