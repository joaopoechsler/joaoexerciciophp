<?
Class administradora extends Dao{
    protected $dadosAdm = array();
    protected $id;

    function __construct(){
    
    }

    function getAdministradora($id = null){
        $qry = 'SELECT 
        adms.id,
        adms.nomeAdm,
        adms.cnpj,
        adms.dataCadastro
        FROM jp_administradora adms';

        if($id){
            $qry .= ' WHERE id='.$id;
            $unique = true;
        }

        return $this->listarData($qry, $unique, 5);
    }

    function setAdministradora($dadosAdm) {
        $values = '';
        $sql = 'INSERT INTO jp_administradora (';
        foreach($dadosAdm as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';

        return $this->insertData($sql);
    
    }

    function editAdministradora($dadosAdm){
        $sql = 'UPDATE jp_administradora SET';

        foreach($dadosAdm as $ch=>$value){
            if($ch != 'editar'){
                $sql .="`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql,', ');
        $sql .=' WHERE id='.$dadosAdm['editar'];

        return $this->updateData($sql);
    }

    function deletaAdministradora($id) {
        $sql = 'DELETE FROM jp_administradora WHERE id ='.$id;
        return $this->deletar($sql);
    }

}
?>