<?
include "uteis.php";

$user = new Restrito();
if (!$user->acesso()) {
  header("Location: login.php");
}

if ($_GET['page'] == 'logout') {
  if ($user->logout()) {
    header('Location: ' . $url_site . 'login.php');
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="img/person-circle.svg" type="image/x-icon" />
  <link rel="stylesheet" href="<?= $url_site ?>css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= $url_site ?>css/app.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
  <title>Cadastro</title>
</head>

<body class="bg-dark">
  
  <nav class="navbar navbar-expand-lg navbar-collapse navbar-dark bg-info justify-content-center">
    <a class="navbar-brand" href="<?= $url_site ?>"><i class="bi bi-person-check-fill" style="font-size: 2rem;"></i></a>
    <div class="nav-item dropdown">
      <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
        Moradores
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="<?= $url_site ?>cadastroMoradores">Cadastrar Moradores</a>
        <a class="dropdown-item" href="<?= $url_site ?>moradores">Listar Moradores</a>
      </div>
    </div>
    <div class="nav-item dropdown">
      <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
        Administradoras
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="<?= $url_site ?>cadastroAdm">Cadastrar Adm</a>
        <a class="dropdown-item" href="<?= $url_site ?>administradora">Listar Adms</a>
      </div>
    </div>
    <div class="nav-item dropdown">
      <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
        Condominios
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="<?= $url_site ?>cadastroCondominio">Cadastrar Condominio</a>
        <a class="dropdown-item" href="<?= $url_site ?>condominios">Listar Condominios</a>
      </div>
    </div>
    <div class="nav-item dropdown">
      <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
        Blocos
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="<?= $url_site ?>cadastroBloco">Cadastrar Bloco</a>
        <a class="dropdown-item" href="<?= $url_site ?>bloco">Listar Bloco</a>
      </div>
    </div>
    <div class="nav-item dropdown">
      <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
        Unidades
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="<?= $url_site ?>cadastroUnidade">Cadastrar Unidades </a>
        <a class="dropdown-item" href="<?= $url_site ?>unidade">Listar Unidades</a>
      </div>
    </div>
    <div class="nav-item dropdown">
      <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
        Conselho
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="<?= $url_site ?>cadastroConselho">Cadastrar Conselho </a>
        <a class="dropdown-item" href="<?= $url_site ?>conselho">Listar Conselho</a>
      </div>
    </div>
    <div class="nav-item dropdown">
      <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
        Usuarios
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="<?= $url_site ?>cadastroUsuario">Cadastrar Usuario </a>
        <a class="dropdown-item" href="<?= $url_site ?>usuarios">Listar Usuarios</a>
      </div>
    </div>
    <a href="<?= $url_site ?>logout" style="font-size: 2rem; margin-left: 0.5em;"><i class="bi bi-box-arrow-left text-light float-right"></i></a>
  </nav>

  <main class="container mt-2 mb-5">
    <?
    switch ($_GET['page']) {
      case '':
      case 'inicio':
        require "controllers/inicio.php";
        require "views/inicio.php";
        break;
      default:
        require 'controllers/' . $_GET['page'] . '.php';
        require 'views/' . $_GET['page'] . '.php';
        break;
    }
    ?>
  </main>

  <footer class="footer fixed-bottom bg-info">
    <span class="pl-2 text-light">2022</span>
    <i class="bi bi-person-check-fill text-light float-right mr-2"></i>
  </footer>
  <script>
    var url_site = '<?= $url_site ?>'
  </script>
  <script src="<?= $url_site ?>js/jquery-3.6.0.min.js"></script>
  <script src="<?= $url_site ?>js/bootstrap.bundle.min.js"></script>
  <script src="<?= $url_site ?>js/jquery.mask.min.js"></script>
  <script src="<?= $url_site ?>js/app.js?v=<?= rand(0, 99999) ?>"></script>
</body>

</html>