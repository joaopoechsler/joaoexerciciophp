<?
include "uteis.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="img/person-circle.svg" type="image/x-icon" />
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/app.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
  <title>Login</title>
</head>

<body class="bg-dark">

  <main class="container mt-5 mb-5">
    <div class="row justify-content-center">
      <div>
          <h1 class="text-light mt-2">Sistema Gerenciador de Condominios</h1>
      </div>
      <form class="col col-6 bg-info rounded pb-5 pt-5 shadow-lg" method="POST" action="<?=$url_site?>/controllers/restrito.php" style="margin-top: 9em;">
      <h1 class="text-center text-light mb-3">Login</h1>
        <div class="form-group mb-3 mt-3">
          <input class="form-control bg-dark text-light border-info border-light" name="usuario" type="text" class="form-control">
        </div>
        <div class="form-group mb-3 mt-3 text-light">
          <input class="form-control bg-dark text-light border-info border-light" name="senha" type="password" class="form-control">
        </div>
        <div class="text-center mb-3 mt-3 text-light">
          <button type="submit" class="btn btn-dark">Login</button>
        </div>
      </form>
    </div>
  </main>

  <script src="js/jquery-3.6.0.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/app.js?v=<?= rand(0, 99999) ?>"></script>
  <?if(isset($_GET['msg'])) {?>
  <script type="text/javascript">
      $(function(){
          myAlert('danger','<?=$_GET['msg']?>','main')
      })
  </script>
  <?}?>
</body>

</html>