<h1 class="text-center text-light">Listagem de Condominios</h1>

<table class="container-fluid table table-dark table-striped table-responsive mt-4 shadow" id="listaCondo">
    <tr>
        <td>Nome</td>
        <td>Blocos</td>
        <td>Rua</td>
        <td>N°</td>
        <td>Bairro</td>
        <td>Cidade</td>
        <td>Estado</td>
        <td>Cep</td>
        <!-- <td>Sindico</td> -->
        <td>Data Cadastro</td>
        <td>Data Att</td>
        <td align="center"><a href="<?=$url_site?>cadastroCondominio" class="btn btn-info">Cadastrar</a></td>
    </tr>

    <?
    foreach ($result['resultSet'] as $chDados => $dadosCondo) {
    ?>
        <tr data-id="<?=$dadosCondo['id']?>">
            <td><?= $dadosCondo['nome'] ?></td>
            <td><?= $dadosCondo['qtblocos'] ?></td>
            <td><?= $dadosCondo['rua'] ?></td>
            <td><?= $dadosCondo['num'] ?></td>
            <td><?= $dadosCondo['bairro'] ?></td>
            <td><?= $dadosCondo['cidade'] ?></td>
            <td><?= $estados[$dadosCondo['estado']] ?></td>
            <td><?= $dadosCondo['cep'] ?></td>
            <td><?= dateFormat($dadosCondo['dataCadastro']) ?></td>
            <td><?= dateFormat($dadosCondo['dataUpdate']) ?></td>
            <td align="center">
                <a href="<?=$url_site?>cadastroCondominio/id/<?= $dadosCondo['id'] ?>"><i class="bi bi-pencil-square text-info"></i></a>
                <a href="#" data-id="<?= $dadosCondo['id'] ?>" class="removerCondo"><i class="bi bi-trash-fill text-info"></i></a>
            </td>
        </tr>
    <? } ?>
    <tr>
        <td colspan="5">&nbsp;</td>
        <td class="totalRegistros" colspan="8" align="right">Total Registros: <?=$totalCadastro?></td>
    </tr>
</table>

<div class="row">
    <div class="col col-12">
        <?=$paginacao?>
    </div>
</div>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['condominio'][$_GET['deletar']]);
    header("Location: index.php?page=condominios");
}
?>