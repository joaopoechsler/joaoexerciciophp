<div class="row justify-content-center">

    <h1 class="text-center text-light">Cadastro de Unidades</h1>

            <form id="form-unidade" class="col-12 mt-4 mb-4 shadow" action="" method="post">

            <select name="from_condominio" class="fromCondominio custom-select form-control">
                    <option value="">Selecione um Condomínio</option>
                        <? foreach($listCondominio['resultSet'] as $condominios){
                            echo '<option value="'.$condominios['id'].'"'.($condominios['id'] == $popular['from_condominio'] ? 'selected' : '').'>'.$condominios['nome'].'</option>';
                        }?>
                </select>

                <select name="from_bloco" class="fromBloco custom-select form-control mt-2">
                        <? if($_GET['id']) {
                            $bloco = new bloco();
                            $blocos = $bloco->getBlocoFromCond($popular['from_condominio']);
                            foreach ($blocos['resultSet'] as $bloco) {
                        ?>
                            <option value="<?=$bloco['id']?>" <?=($bloco['id'] == $popular['from_bloco'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
                        <?  } } ?>
                </select>
                
                <input class="col col-12 mt-2 form-control" type="text" name="numUnd" value="<?= $popular['numUnd'] ?>" placeholder="Numero da unidade" required>
                <input class="col col-12 mt-2 form-control" type="text" name="metragem" value="<?= $popular['metragem'] ?>" placeholder="Metragem"required>
                <input class="col col-12 mt-2 form-control" type="number" name="qtVagas" value="<?= $popular['qtVagas'] ?>" placeholder="Qtd de Vagas">
                <? if ($_GET['id']) { ?>
                    <input type="hidden" name="editar" value="<?= $_GET['id'] ?>">
                <? } ?>
                <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button>
            </form>

</div>