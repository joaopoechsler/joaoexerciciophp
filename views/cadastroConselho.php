<div class="row justify-content-center">

    <h1 class="text-center text-light">Cadastro de conselhos</h1>

    <form id="form-conselho" class="col-12 mt-4 mb-4 shadow" action="" method="post">

        <input class="col col-12 mt-2 form-control" type="text" name="NomeFunc" value="<?= $popular['NomeFunc'] ?>" placeholder="Nome" required>

        <select name="from_condominio" class="fromCondominio custom-select form-control mt-2">
            <option value="">Selecione um Condomínio</option>
            <? foreach ($listCondominio as $condominios) {
                echo '<option value="' . $condominios['id'] . '"' . ($condominios['id'] == $popular['id'] ? 'selected' : '') . '>' . $condominios['nome'] . '</option>';
            } ?>
        </select>

        <select name="funcao" class="fromCondominio custom-select form-control mt-2">
            <option value="">Selecione uma Funcao</option>
            <? foreach ($listarFuncao['resultSet'] as $conselho) {
                echo '<option value="' . $conselho['funcao'] . '"' . ($conselho['funcao'] == $popular['funcao'] ? 'selected' : '') . '>' . $conselho['funcao'] . '</option>';
            } ?>
        </select>

        <? if ($_GET['id']) { ?>
            <input type="hidden" name="editar" value="<?= $_GET['id'] ?>">
        <? } ?>
        <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button>
    </form>

</div>