<h1 class="text-center text-light">Listagem de Administradoras</h1>

<table class="col col-md-12 col-sm-10 table table-dark table-striped table-responsive-md table-responsive-lg mt-4 shadow" id="listaAdm">
    <tr>
        <td>Nome</td>
        <td>Documento</td>
        <td>Data Cadastro</td>
        <td>Data Atualizado</td>
        <td align="center"><a href="<?=$url_site?>cadastroAdm" class="btn btn-info">Cadastrar</a></td>
    </tr>

    <?
    foreach ($result['resultSet'] as $dadosAdm) {
    ?>
        <tr data-id="<?=$dadosAdm['id']?>">
            <td><?= $dadosAdm['nomeAdm'] ?></td>
            <td><?= $dadosAdm['cnpj'] ?></td>
            <td><?= dateFormat($dadosAdm['dataCadastro']) ?></td>
            <td><?= dateFormat($dadosAdm['dataUpdate']) ?></td>
            <td align="center">
                <a href="<?=$url_site?>cadastroAdm/id/<?= $dadosAdm['id'] ?>"><i class="bi bi-pencil-square text-info"></i></a>
                <a href="#" data-id="<?= $dadosAdm['id'] ?>" class="removerAdm"><i class="bi bi-trash-fill text-info"></i></a>
            </td>
        </tr>
    <? } ?>
    <tr>
        <td colspan="3">&nbsp;</td>
        <td class="totalRegistros" colspan="2" align="right">Total Registros: <?=$totalRegistros?></td>
    </tr>
</table>

<div class="row">
    <div class="col col-12">
        <?=$paginacao?>
    </div>
</div>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['cadastro'][$_GET['deletar']]);
    header("Location: index.php?page=administradora");
}
?>