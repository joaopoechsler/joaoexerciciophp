<h1 class="text-center text-light">Listagem de Usuarios</h1>

<table class="container-fluid table table-dark table-striped table-responsive-md table-responsive-lg mt-4 shadow" id="listaUnd">
    <tr>
        <td>Nome</td>
        <td>Username</td>
        <td>Data Criado</td>
        <td>Data Atualizada</td>
        <td align="center"><a href="index.php?page=cadastroUsuarios" class="btn btn-info">Cadastrar</a></td>
    </tr>

    <?
    foreach ($result['resultSet'] as $dadosUser) {
    ?>
        <tr data-id="<?=$dadosUser['id']?>">
            <td><?= $dadosUser['nome'] ?></td>
            <td><?= $dadosUser['usuario'] ?></td>
            <td><?= dateFormat($dadosUser['dataCadastro']) ?></td>
            <td><?= dateFormat($dadosUser['dataUpdate']) ?></td>
            <td align="center">
                <a href="<?=$url_site?>cadastroUsuarios/id/<?= $dadosUser['id'] ?>"><i class="bi bi-pencil-square text-info"></i></a>
                <a href="#" data-id="<?= $dadosUser['id']?>" class="removerUser"><i class="bi bi-trash-fill text-info"></i></a>
            </td>
        </tr>
    <? } ?>
    <tr>
        <td colspan="4">&nbsp;</td>
        <td class="totalRegistros" colspan="4" align="right">Total Registros: <?=$totalRegistros?></td>  
    </tr>
</table>

<div class="row">
    <div class="col col-12">
        <?=$paginacao?>
    </div>
</div>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['unidade'][$_GET['deletar']]);
    header("Location: index.php?page=unidade");
}
?>