<div class="row justify-content-center">

    <h1 class="text-center text-light">Cadastro de Bloco</h1>
            <form id="form-bloco" class="col-12 mt-4 mb-4 shadow" action="" method="post">

            <select name="from_condominio" class="fromCondominio custom-select form-control">
                    <option value="">Selecione um Condomínio</option>
                        <? foreach($listCondominio['resultSet'] as $condominios){
                            echo '<option value="'.$condominios['idCondo'].'"'.($condominios['id'] == $popular['idCondo'] ? 'selected' : '').'>'.$condominios['nome'].'</option>';
                        }?>
                </select>

                <input class="col col-12 mt-2 form-control" type="text" name="nomeBloco" value="<?= $popular['nomeBloco'] ?>" placeholder="Nome do Bloco" required>
                <input class="col col-12 mt-2 form-control" type="text" name="Andares" value="<?=  $popular['Andares'] ?>" placeholder="Andares"required>
                <input class="col col-12 mt-2 form-control" type="text" name="qtUnidadesAndar" value="<?=  $popular['qtUnidadesAndar'] ?>" placeholder="Qtd de Unidades por Andar">
                <? if ($_GET['id']) { ?>
                    <input type="hidden" name="editar" value="<?= $_GET['id'] ?>">
                <? } ?>
                <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button>
            </form>
</div>