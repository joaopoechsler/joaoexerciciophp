<h1 class="text-center text-light">Listagem de blocos</h1>

<table class="container-fluid table table-dark table-striped table-responsive-md table-responsive-lg mt-4 shadow" id="listaBloco">
    <tr>
        <td>Nome</td>
        <td>Andares</td>
        <td>Qtd por Andar</td>
        <td>Condominio</td>
        <td>Data Criado</td>
        <td>Data Atualizada</td>
        <td align="center"><a href="<?=$url_site?>cadastroBloco" class="btn btn-info">Cadastrar</a></td>
    </tr>

    <?
    foreach ($result['resultSet'] as $dadosBloco) {
    ?>
        <tr data-id="<?=$dadosBloco['id']?>">
            <td><?= $dadosBloco['nomeBloco'] ?></td>
            <td><?= $dadosBloco['Andares'] ?></td>
            <td><?= $dadosBloco['qtUnidadesAndar'] ?></td>
            <td><?= $dadosBloco['nome'] ?></td>
            <td><?= dateFormat($dadosBloco['dataCadastro']) ?></td>
            <td><?= dateFormat($dadosBloco['dataUpdate']) ?></td>
            <td align="center"> 
                <a href="<?=$url_site?>cadastroBloco/id/<?= $dadosBloco['id'] ?>"><i class="bi bi-pencil-square text-info"></i></a>
                <a href="#" data-id="<?= $dadosBloco['id']?>" class="removerBloco"><i class="bi bi-trash-fill text-info"></i></a>
            </td>
        </tr>
    <? } ?>

    <tr>
        <td colspan="5">&nbsp;</td>
        <td class="totalRegistros" colspan="8" align="right">Total Registros: <?=$totalCadastro?></td>
    </tr>
</table>

<div class="col col-8">
    <?=$paginacao?>
</div>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['bloco'][$_GET['deletar']]);
    header("Location: index.php?page=bloco");
}
?>