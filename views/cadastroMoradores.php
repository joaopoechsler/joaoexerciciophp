<div class="">

    <h1 class="text-center text-light">Cadastro de Moradores</h1>
    <form id="form-clientes">

        <select name="from_condominio" class="fromCondominio custom-select form-control">
            <option value="">Selecione um Condomínio</option>
            <? foreach ($listCondominio['resultSet'] as $condominios) {
                echo '<option value="' . $condominios['id'] . '"' . ($condominios['id'] == $popular['from_condominio'] ? 'selected' : '') . '>' . $condominios['nome'] . '</option>';
            } ?>
        </select>

        <select name="from_bloco" class="fromBloco custom-select form-control mt-2">
            <? if ($_GET['id']) {
                $blocos = $morador->getBlocoFromCond($popular['from_bloco']);
                foreach ($blocos['resultSet'] as $bloco) {
            ?>
                    <option value="<?= $bloco['id'] ?>" <?= ($bloco['id'] == $popular['from_bloco'] ? 'selected' : '') ?>><?= $bloco['nomeBloco'] ?></option>
            <?  }
            } ?>
        </select>

        <select name="from_unidade" class="fromUnidade custom-select form-control mt-2">
            <? if ($_GET['id']) {
                $unidades = $morador->getUnidadesFromBloco($popular['from_unidade']);
                foreach ($unidades['resultSet'] as $unidade) {
            ?>
                    <option value="<?= $unidade['id'] ?>" <?= ($unidade['id'] == $popular['from_unidade'] ? 'selected' : '') ?>><?= $unidade['numUnd'] ?></option>
            <?  }
            } ?>
        </select>

        <input class="col col-12 mt-2 form-control" type="text" name="nomeMorador" value="<?= $popular['nomeMorador'] ?>" placeholder="Nome" required>
        <input class="col col-12 mt-2 form-control" type="text" name="CPF" value="<?= $popular['cpf'] ?>" placeholder="CPF" required>
        <input class="col col-12 mt-2 form-control" type="email" name="email" value="<?= $popular['email'] ?>" placeholder="Email" required>
        <input class="col col-12 mt-2 form-control" type="text" name="telefone" value="<?= $popular['telefone'] ?>" placeholder="Telefone">
        <? if ($_GET['id']) { ?>
            <input type="hidden" name="editar" value="<?= $_GET['id'] ?>">
        <? } ?>
        <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button>
    </form>
</div>

</div>