<div class="mt-2 mb-4">
    <?
    $nome = explode(' ', $_SESSION['USUARIO'])
    ?>
    <h1 class="text-light text-center">Olá <?= $_SESSION['USUARIO']['nome']; ?>, Seja Bem Vindo(a).</h1>
</div>

<div class="row justify-content-center">

    <div class="col col-6 col-sm-4 mb-2">
        <div class="card-body justify-content-center">
            <h5 class="card-title h3 text-light text-center">Qtd Moradores</h5>
            <?
            foreach ($dash['resultSet'] as $value) {
            ?>
                <ul class="list-group bg-info p-2 mt-2">
                    <li class="list-group-item d-flex justify-content-between align-items-center bg-dark text-light">
                        <?= $value['nome'] ?>
                        <span class="badge badge-info"><?= $value['totalMoradores'] ?></span>
                    </li>
                </ul>
            <? } ?>
        </div>
    </div>

    <div class="col col-6 col-sm-4 mb-2">

        <div class="card-body justify-content-center">
            <h5 class="card-title h3 text-light text-center">Últimas Adms:</h5>
            <?
            foreach ($admTotal['resultSet'] as $value) {
            ?>
                <ul class="list-group bg-info p-2 mt-2">
                    <li class="list-group-item d-flex justify-content-between align-items-center bg-dark text-light">
                        <?= $value['nome'] ?>
                        <span><?= $value['nomeAdm'] ?></span>
                    </li>
                </ul>
            <? } ?>
        </div>
    </div>

</div>

<div class="row text-center card-group col-12 m-auto">

    <div class="card ml-2 bg-info text-light rounded" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">Administradoras</h5>
            <?
            $adm = new administradora();
            $result = $adm->getAdministradora()
            ?>
            <p class="card-text h1">
                <td colspan="2" align="left" class="totalRegistros"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></td>
            </p>
        </div>
    </div>

    <div class="card ml-2 bg-info text-light rounded" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">Condomínios</h5>
            <?
            $condominio = new condominio();
            $result = $condominio->getCondo()
            ?>
            <p class="card-text h1">
                <td colspan="2" align="left" class="totalRegistros"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></td>
            </p>
        </div>
    </div>

    <div class="card ml-2 bg-info text-light rounded" style="width: 18rem;">
        <div class="card-body">
            <?
            $bloco = new bloco();
            $result = $bloco->getBloco();
            ?>
            <h5 class="card-title">Blocos</h5>
            <p class="card-text h1">
                <td colspan="2" align="left" class="totalRegistros"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></td>
            </p>
        </div>
    </div>

    <div class="card ml-2 bg-info text-light rounded" style="width: 18rem;">
        <div class="card-body">
            <?
            $unidade = new unidade();
            $result = $unidade->getUnidade();
            ?>
            <h5 class="card-title">Unidades</h5>
            <p class="card-text h1">
                <td colspan="2" align="left" class="totalRegistros"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></td>
            </p>
        </div>
    </div>

    <div class="card ml-2 bg-info text-light rounded" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">Moradores</h5>
            <?
            $condomino = new Cadastro();
            $result = $condomino->getMorador();
            ?>
            <p class="card-text h1">
                <td colspan="2" align="left" class="totalRegistros"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></td>
            </p>
        </div>
    </div>

</div>