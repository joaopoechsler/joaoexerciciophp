<div class="row justify-content-center">

    <h1 class="text-center text-light">Cadastro de Condominios</h1>

    <form id="form-condo" class="col-12 mt-4 mb-4 shadow" action="" method="post">
        <input type="hidden" name="from_administradora" value="1">
        <input type="hidden" name="from_bloco" value="1">
        <input type="hidden" name="from_sindico" value="1">
        <input class="col col-12 mt-2 form-control" type="text" name="nome" value="<?= $popular['nome'] ?>" placeholder="Nome" required>
        <input class="col col-12 mt-2 form-control" type="text" name="qtblocos" value="<?= $popular['qtblocos'] ?>" placeholder="Quantidade de Blocos" required>

            <br>
            <br>
            <input class="col col-12 mt-2 form-control" type="text" name="rua" value="<?= $popular['rua'] ?>" placeholder="Rua" required>
            <input class="col col-12 mt-2 form-control" type="text" name="num" value="<?= $popular['num'] ?>" placeholder="Número" required>
            <input class="col col-12 mt-2 form-control" type="text" name="bairro" value="<?= $popular['bairro'] ?>" placeholder="Bairro" required>
            <input class="col col-12 mt-2 form-control" type="text" name="cidade" value="<?= $popular['cidade'] ?>" placeholder="Cidade" required>

            <select class="col-12 custom-select mt-2" name="estado" id="">
                <option value="">Selecione o Estado</option>
                <? foreach ($estados as $sig => $uf) { ?>
                    <option value="<?= $sig ?>" <?= ($sig == $popular['estado'] ? 'selected="selected"' : '') ?>><?= $uf ?></option>
                <? } ?>
            </select>

            <input class="col col-12 mt-2 form-control" type="text" name="cep" value="<?= $popular['cep'] ?>" placeholder="Cep" required>

        <? if ($_GET['id']) { ?>
            <input type="hidden" name="editar" value="<?= $_GET['id'] ?>">
        <? } ?>
        <button class="btn btn-info mt-2 col col-12 buttonEnviar" type="submit">Enviar</button>
    </form>
</div>