<?
$condominio = new condominio();
$listCondominio = $condominio->getCondo();
?>

<h1 class="text-center text-light">Listagem de Moradores</h1>

<table class="col col-md-12 col-sm-10 table table-dark table-striped table-responsive-md table-responsive-lg mt-4 shadow" id="listaClientes">

    <tr>
        <td colspan="12">
            <form class="form-inline my-2 my-lg-0" action="<?=$url_site?>index.php" method="GET" id="filtro">
                <input type="hidden" name="page" value="moradores">
                <input class="form-control bg-dark text-light border-info mr-sm-2 termo1" type="search" placeholder="Buscar por Nome" aria-label="Search" name="b[nomeMorador]">
                
                <select name="b[morador.from_condominio]" class="custom-select bg-dark text-light border-info mr-sm-2 termo2">
                    <option value="">Filtrar por Condomínio</option>
                        <? foreach($listCondominio['resultSet'] as $condominios){
                            echo '<option value="'.$condominios['id'].'">'.$condominios['nome'].'</option>';
                        }?>
                </select>

                <button class="btn btn-info my-2 my-sm-0 busca" type="submit" disabled>Buscar</button>
                <a class="pl-2 text-info my-2 my-sm-0" href="<?=$url_site?>moradores"><i class="bi bi-x-square" style="font-size: 1.25rem;"></i></a>
            </form>
        </td>
    </tr>

    <tr>
        <td>Condominio</td>
        <td>Bloco</td>
        <td>Unidade</td>
        <td>Nome</td>
        <td>Documento</td>
        <td>Email</td>
        <td>Telefone</td>
        <td>Data Cadastro</td>
        <td>Data Atualizado</td>
        <td align="center"><a href="<?=$url_site?>cadastroMoradores" class="btn btn-info">Cadastrar</a></td>
    </tr>

    <?
    foreach ($result['resultSet'] as $dados) {
    ?>
        <tr data-id="<?=$dados['id']?>">
            <td><?= $dados['nome'] ?></td>
            <td><?= $dados['nomeBloco'] ?></td>
            <td><?= $dados['numUnd'] ?></td>
            <td><?= $dados['nomeMorador'] ?></td>
            <td><?= $dados['cpf'] ?></td>
            <td><?= $dados['email'] ?></td>
            <td><?= $dados['telefone'] ?></td>
            <td><?= dateFormat($dados['dataCadastro']) ?></td>
            <td><?= dateFormat($dados['dataUpdate']) ?></td>
            <td align="center">
                <a href="<?=$url_site?>cadastroMoradores/id/<?= $dados['id'] ?>"><i class="bi bi-pencil-square text-info"></i></a>
                <a href="#" data-id="<?= $dados['id'] ?>" class="removerCliente"><i class="bi bi-trash-fill text-info"></i></a>
            </td>
        </tr>
    <? } ?>
    
    <tr>
        <td colspan="5">&nbsp;</td>
        <td class="totalRegistros" colspan="6" align="right">Total Registros: <?=$totalRegistros?></td>
    </tr>
</table>    

<div class="row">
    <div class="col col-12">
        <?=$paginacao?>
    </div>
</div>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['cadastro'][$_GET['deletar']]);
    header("Location: index.php?page=clientes");
}
?>