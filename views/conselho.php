<h1 class="text-center text-light">Listagem de Conselho</h1>

<table class="col col-md-12 col-sm-10 table table-dark table-striped table-responsive-md table-responsive-lg mt-4 shadow" id="listaConselho">
    <tr>
        <td>Nome</td>
        <td>Funcao</td>
        <td>Condominio</td>
        <td>Data Cadastro</td>
        <td>Data Atualizado</td>
        <td align="center"><a href="<?=$url_site?>cadastroConselho" class="btn btn-info">Cadastrar</a></td>
    </tr>

    <?
    foreach ($result['resultSet'] as $dadosConselho) {
    ?>
        <tr data-id="<?=$dadosConselho['id']?>">
            <td><?= $dadosConselho['NomeFunc'] ?></td>
            <td><?= $dadosConselho['funcao'] ?></td>
            <td><?= $dadosConselho['nome'] ?></td>
            <td><?= dateFormat($dadosConselho['dataCadastro']) ?></td>
            <td><?= dateFormat($dadosConselho['dataUpdate']) ?></td>
            <td align="center">
                <a href="<?=$url_site?>cadastroConselho/id/<?=$dadosConselho['id'] ?>"><i class="bi bi-pencil-square text-info"></i></a>
                <a href="#" data-id="<?=$dadosConselho['id'] ?>" class="removerConselho"><i class="bi bi-trash-fill text-info"></i></a>
            </td>
        </tr>
    <? } ?>
    <tr>
        <td colspan="4">&nbsp;</td>
        <td class="totalRegistros" colspan="2" align="right">Total Registros: <?=$totalRegistros?></td>
    </tr>
</table>

<div class="row">
    <div class="col col-12">
        <?=$paginacao?>
    </div>
</div>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['conselho'][$_GET['deletar']]);
    header("Location: index.php?page=conselho");
}
?>