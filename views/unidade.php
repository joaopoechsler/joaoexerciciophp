<h1 class="text-center text-light">Listagem de Unidades</h1>

<table class="container-fluid table table-dark table-striped table-responsive-md table-responsive-lg mt-4 shadow" id="listaUnd">
    <tr>
        <td>Condominio</td>
        <td>Bloco</td>
        <td>Numero</td>
        <td>Metragem</td>
        <td>Vagas</td>
        <td>Data Criado</td>
        <td>Data Atualizada</td>
        <td align="center"><a href="index.php?page=cadastroUnidade" class="btn btn-info">Cadastrar</a></td>
    </tr>

    <?
    foreach ($result['resultSet'] as $dadosUnd) {
    ?>
        <tr data-id="<?=$dadosUnd['id']?>">
            <td><?= $dadosUnd['nome'] ?></td>
            <td><?= $dadosUnd['nomeBloco'] ?></td>
            <td><?= $dadosUnd['numUnd'] ?></td>
            <td><?= $dadosUnd['metragem'] ?></td>
             <td><?= $dadosUnd['qtVagas'] ?></td>
            <td><?= dateFormat($dadosUnd['dataCadastro']) ?></td>
            <td><?= dateFormat($dadosUnd['dataUpdate']) ?></td>
            <td align="center">
                <a href="<?=$url_site?>cadastroUnidade/id/<?= $dadosUnd['id'] ?>"><i class="bi bi-pencil-square text-info"></i></a>
                <a href="#" data-id="<?= $dadosUnd['id']?>" class="removerUnd"><i class="bi bi-trash-fill text-info"></i></a>
            </td>
        </tr>
    <? } ?>
    <tr>
        <td colspan="5">&nbsp;</td>
        <td class="totalRegistros" colspan="8" align="right">Total Registros: <?=$totalRegistros?></td>  </tr>
</table>

<div class="row">
    <div class="col col-12">
        <?=$paginacao?>
    </div>
</div>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['unidade'][$_GET['deletar']]);
    header("Location: index.php?page=unidade");
}
?>